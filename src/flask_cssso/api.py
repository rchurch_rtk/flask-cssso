import os
import requests
import jwt
import random
import time
import urllib

class CSSSOApi():
    def __init__(self, app_code, cipher_key, api_url):
        self.app_code = app_code
        self.cipher_key = cipher_key
        self.api_url = api_url
        self.user_memos = {}

    def nonce(self):
        return (int(time.time()) << 48) + \
            (int(os.getpid() % 65535) << 32) + \
            (random.randint(0, (2**32) - 1))

    def encode_jwt(self, sub, exp=None):
        ten_minutes = 10 * 60
        exp = exp if exp else int(time.time()) + ten_minutes
        return jwt.encode(
            dict(iss=self.app_code, sub=sub, jti=self.nonce(),
                 exp=exp),
            self.cipher_key,
            algorithm='HS256').decode('ascii')

    def decode_jwt(self, token):
        return jwt.decode(token, self.cipher_key, alogorithm='HS256',
                          audience=self.app_code)
    
    def api_request(self, command, **args):
        uri = urllib.parse.urljoin(self.api_url, 'Api/' + command) + \
            ('?' + urllib.parse.urlencode(args) if args else '')
        return requests.get(uri).json()

    def build_login_url(self, return_url):
        return '?' + \
            urllib.parse.urlencode(
                dict(v=1, a=self.app_code, p=return_url))
    
    def login_url(self, return_url):
        return self.api_request('LoginUrl')['loginUrl'] + \
            self.build_login_url(return_url)

    def logout_url(self, return_url):
        return self.api_request('LogoutUrl')['logoutUrl'] + \
            self.build_login_url(return_url)

    def user(self, sub):
        return self.api_request('User', jwt=self.encode_jwt(sub))

    def memoized_user(self, sub):
        user = self.user_memos.get(sub)
        if not user:
            user = self.user(sub)
            self.user_memos[sub] = user
        return user
    
    def session_is_valid(self, sub):
        return bool(self.api_request('Session', sub=sub)['isValid'])
