from flask_cssso.api import CSSSOApi
from flask import current_app, redirect, request, session
from functools import wraps
import urllib.parse

class FlaskCSSSO():
    def init_app(self, app):
        self.api = CSSSOApi(
            app.config['CSSSO_APP_CODE'],
            app.config['CSSSO_CIPHER_KEY'],
            app.config['CSSSO_API_URL'])
        app.config.setdefault('CSSSO_SESSION_SUBJECT_KEY', 'cssso_sub')

    def has_subject(self):
        return current_app.config['CSSSO_SESSION_SUBJECT_KEY'] in session

    def subject(self):
        return session.get(current_app.config['CSSSO_SESSION_SUBJECT_KEY'])

    def set_subject(self, sub):
        session[current_app.config['CSSSO_SESSION_SUBJECT_KEY']] = sub

    def url_without_jwt(self):
        parts = urllib.parse.urlparse(request.url)
        query_without_jwt = urllib.parse.urlencode(
            [elt for elt in \
             urllib.parse.parse_qsl(parts.query, keep_blank_values=True)  \
             if elt[0] != 'jwt'])
        return urllib.parse.urlunparse(
            (parts.scheme, parts.netloc, parts.path, parts.params, \
             query_without_jwt, parts.fragment))

    def user(self):
        return self.api.memoized_user(self.subject())

    def user_id(self):
        return self.user()['userId']

    def client_id(self):
        return self.user()['clientId']
    
    def groups(self):
        return self.user()['groups']

    def has_group(self, group):
        return group in self.groups()
    
    def login_required(self, view_func):
        @wraps(view_func)
        def decorated_view_func(*args, **kwargs):
            if 'jwt' in request.args:
                jwt = self.api.decode_jwt(request.args['jwt'])
                self.set_subject(jwt['sub'])
                return redirect(self.url_without_jwt())
            elif self.has_subject() and self.api.session_is_valid(self.subject()):
                return view_func(*args, **kwargs)
            else:
                return redirect(self.api.login_url(request.url))
        return decorated_view_func
            
    def logout(self, url=None):
        return redirect(self.api.logout_url(url or request.url_root))
