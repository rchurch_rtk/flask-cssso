# This is a 'hello world' app to demo the CSSSO extension

from flask import Flask
from flask_cssso import FlaskCSSSO


DEBUG = True
SECRET_KEY = 'development key'

CSSSO_APP_CODE = 'asssopoc'
CSSSO_API_URL = 'https://int-auth-jwt.office.comscore.com/'
# CSSSO_CIPHER_KEY = 'actually secret! be careful with this!'


app = Flask(__name__)
app.config.from_object(__name__)


sso = FlaskCSSSO()
sso.init_app(app)

@app.route('/')
@sso.login_required
def hello():
    return 'hello {}'.format(sso.user_id())

@app.route('/logout')
@sso.login_required
def logout():
    return sso.logout()
