Flask-CSSSO
====

This is a Flask extension to use the Comscore Single Sign On API in
Python Flask apps.


Resources
----

The Comscore Single Sign On API is described in these two documents:

[Overview](https://tst-auth-jwt.office.comscore.com/Doc/Overview)
[API Methods](https://tst-auth-jwt.office.comscore.com/Doc/ApiMethods)